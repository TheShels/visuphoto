
      <?php
				//include 'errors.php';
				function ResizeImg($img, $to,$signaturePhoto){
					$ImageChoisie = imagecreatefromjpeg($img);

					$TailleImageChoisie = getimagesize($img);
									
					// Definition de la largeur fixée
					$NouvelleLargeur = 2048;
					 
					// coefficent reducteur
					$Reduction = ( ($NouvelleLargeur * 100)/$TailleImageChoisie[0] );
					 
					// calcul de la hauteur adaptée a la largeur
					$NouvelleHauteur = ( ($TailleImageChoisie[1] * $Reduction)/100 );
					
					//on créé la nouvelle image vide avec les nouvelles dimensions
					$NouvelleImage = imagecreatetruecolor($NouvelleLargeur , $NouvelleHauteur) or die ("Erreur");
					 
					//on copie l'image source dans la nouvelle image
					imagecopyresampled($NouvelleImage , $ImageChoisie, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0],$TailleImageChoisie[1]);
					
					
					//date de prise de la photo
					$exif = exif_read_data($img, 'IFD0');
					$datePhoto = $exif['DateTime'];						
					if($datePhoto != ''){	
						//concatenation info patient et date de prise de photo
						$textePhoto = $signaturePhoto . ' (Photo du : ' .$datePhoto .')';
					}
					else{
						$textePhoto = $signaturePhoto;
					}
				
						
					
					// D'abord, nous créons un rectangle contenant notre texte
					$bbox = imageftbbox(35, 0, './arial.ttf', $textePhoto);
					$stamp = imagecreatetruecolor(abs($bbox[2]) + 50,abs($bbox[5]) + 50);
					

					// Nos coordonnées en X et en Y
					$x = $bbox[0] + (imagesx($stamp) / 2) - ($bbox[4] / 2) - 5;
					$y = $bbox[1] + (imagesy($stamp) / 2) - ($bbox[5] / 2) - 5;

					$sign = imagefttext($stamp, 35, 0, $x, $y, 0xFFFF00, './arial.ttf' ,$textePhoto);
					

					// Définit les marges du cachet et récupère la largeur et la hauteur du cachet
					$marge_right = 10;
					$marge_bottom = 10;
					$sx = imagesx($stamp);
					$sy = imagesy($stamp);
					// Fusionne le cachet dans notre photo avec une opacité de 50%
					imagecopymerge($NouvelleImage, $stamp, imagesx($NouvelleImage) - $sx - $marge_right, imagesy($NouvelleImage) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp), 50);

					
					//on sauvegarde la nouvelle image dans le repertoire sepecifié
					imagejpeg($NouvelleImage,$to , 100);
				}				
       ?>
