<?php
   /**
   ***Affichage des dossiers d'un patient selon l'IPP
   **/
   
   //on recupere l'url de la page
     $page = basename($_SERVER['SERVER_NAME'].$_SERVER["REQUEST_URI"]);
   
   /*on recupere les dossier d'un patient*/
     $query = "SELECT * FROM TBLDOSPHOTOS WHERE IPP = ".$ipp." ORDER BY NOMDOS ASC";
     $answer = oci_parse($arnum, $query);
     oci_execute($answer);
     
     $nbsearch=0;
   /*on affiche chaque dossier, si le dossier est celui selectionné par l'utilisateur, on marque celui-ci*/
      while ($row = oci_fetch_array($answer, OCI_BOTH)) {
   $nom = $row[NOMDOS];
   if (strlen($nom)>10){
	$nom = substr($row[NOMDOS],0,10)."...";
   }
   if($page == "visuphotos.php?person=".$ipp."&iddos=".$row[IDDOS].'&user='.$user){
	echo '  <li><a class="active" href="visuphotos.php?person='.$ipp.'&iddos='.$row[IDDOS].'&user='.$user.'" title = "'.$row[NOMDOS].'">'.$nom.'</a></li>'; 
   }
   else{
	echo '  <li><a href="visuphotos.php?person='.$ipp.'&iddos='.$row[IDDOS].'&user='.$user.'" title = "'.$row[NOMDOS].'" >'.$nom.'</a></li>'; 
   }
   $nbsearch++;
      }
   /*fonction permettant le recherche de dossier depuis la liste affichée*/
    echo'<script>
   function rechercheDossier() {
     var input, filter, ul, li, a, i;
     input = document.getElementById("mySearch");
     filter = input.value.toUpperCase();
     ul = document.getElementById("myMenu");
     li = ul.getElementsByTagName("li");
    
     for (i = 0; i < li.length; i++) {
   	a = li[i].getElementsByTagName("a")[0];
   	if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
   	  li[i].style.display = "";
   	} else {
   	  li[i].style.display = "none";
   	}
     }
   }
   </script>';
   ?>