<?php
   /**
   ***Upload de l'image selectionnée par l'utilisateur
   ***(source : https://apprendre-php.com/tutoriels/tutoriel-17-uploader-des-images-sur-un-serveur-web.html)
   ***adapté par kb
   **/

   // Constantes
    // define('TARGET', 'photos\\'.$ipp.'\\'.$iddos.'\\');    // Repertoire cible
     define('TARGET', '\\\\blc087.ch-v.net\\demat\\PHOTOS\\'.$ipp.'\\'.$iddos.'\\');    // Repertoire cible
     define('MAX_SIZE', 10000000);    // Taille max en octets du fichier
     define('WIDTH_MAX', 10000);    // Largeur max de l'image en pixels
     define('HEIGHT_MAX', 10000);    // Hauteur max de l'image en pixels
      
     // Tableaux de donnees
     $tabExt = array('jpg','png','jpeg','gif');    // Extensions autorisees
     $infosImg = array();
      
     // Variables
     $extension = '';
     $message = '';
     $nomImage = '';
     $description = $_POST['description'];
     /************************************************************
      * Creation du repertoire cible si inexistant
      *************************************************************/
     if( !is_dir(TARGET) ) {
       if( !mkdir(TARGET, 0777,true) ) {
         exit('<br> Erreur : le répertoire cible ne peut-être créé . Veuillez contacter le service DESK de la DSI. '.TARGET);
       }
     }

     /************************************************************
      * Script d'upload
      *************************************************************/
     if(!empty($_POST))
     {     			 
       // On verifie si le champ est rempli
       if( !empty($_FILES['fichier']['name']) )
       {
         // Recuperation de l'extension du fichier
         $extension  = pathinfo($_FILES['fichier']['name'], PATHINFO_EXTENSION);
      
         // On verifie l'extension du fichier
         if(in_array(strtolower($extension),$tabExt))
         {
           // On recupere les dimensions du fichier
           $infosImg = getimagesize($_FILES['fichier']['tmp_name']);
			
			
           // On verifie le type de l'image
           if($infosImg[2] >= 1 && $infosImg[2] <= 14)
           {
             // On verifie les dimensions et taille de l'image
             if(filesize($_FILES['fichier']['tmp_name']) <= MAX_SIZE)
             {
               // Parcours du tableau d'erreurs
               if(isset($_FILES['fichier']['error']) 
                 && UPLOAD_ERR_OK === $_FILES['fichier']['error'])
               {
                 // On renomme le fichier
                 $nomImage = pathinfo($_FILES['fichier']['name'], PATHINFO_FILENAME) .'.'. $extension;
      
                 // Si c'est OK, on teste l'upload
                 if(move_uploaded_file($_FILES['fichier']['tmp_name'], TARGET.$nomImage))
                 {
     				$query = "INSERT INTO TBLPHOTOS (idphoto,ipp,nomphoto,auteur,description,datestockage,visible,iddos) VALUES (TBLPHOTOS_ID_SEQ.nextval,'".$ipp."', '".TARGET.$nomImage."','".$user."','".$description."',sysdate,1,".$iddos.")";
					 $answer = oci_parse($arnum, $query);
					 
                     oci_execute($answer);


                     include 'resize.php';
					 ResizeImg(TARGET.$nomImage,TARGET.$nomImage,$signaturePhoto);
     
     			  //echo "<script>alert('Upload réussi !')</script>";
                 }
                 else
                 {
                   // Sinon on affiche une erreur systeme
     			  echo "<script>alert('Problème lors de l\'upload !')</script>";
                 }
               }
               else
               {
     			  echo "<script>alert('Une erreur interne a empêché l\'uplaod de l\'image')</script>";
               }
             }
             else
             {
               // Sinon erreur sur les dimensions et taille de l'image
     			  echo "<script>alert('Erreur dans les dimensions de l\'image !')</script>";
             }
           }
           else
           {
             // Sinon erreur sur le type de l'image
     			  echo "<script>alert('Le fichier à uploader n\'est pas une image !')</script>";
           }
         }
         else
         {
           // Sinon on affiche une erreur pour l'extension
     	  echo "<script>alert('L\'extension du fichier est incorrecte !')</script>";
         }
       }
     }

?>