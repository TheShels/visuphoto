<?php
   /**
   ***affichage des infos patients situées dans le bandeau
   **/
     echo "<div class='Patient'>";
                     //Récupération des données Patients - Nom - Prénom - Nom Marital - Date de Naissance - Sexe
                     $query = "SELECT ETC_NOM, ETC_NOM_MAR, ETC_PRN, ETC_SEX, to_char(ETC_DDN, 'DD/MM/RRRR') FROM pa_pat@Patient WHERE PAT_IPP=" . $ipp;
     
                     $answer = oci_parse($arnum, $query);
                     oci_execute($answer);
     
                     $nbsearch = 0;
                     $signaturePhoto = ''; 
                    /*Affichage des données du patient*/
                     while ($row = oci_fetch_array($answer, OCI_BOTH)) {
                        $row['PAT_IPP'] = $ipp;
     
                         echo "<span class='DPATIPP'><span>IPP : </span> <a href='http://visucourrier2?person=" .$ipp . "' title='Consulter VisuCourrier pour ce patient'>". $ipp ."</a></span>";
     
                         
                         if ($row['ETC_NOM']) {
                             echo "<span class='DPAT'><span> Nom: </span>   ";
                             echo $row['ETC_NOM'];
                             $signaturePhoto = $signaturePhoto . $row['ETC_NOM']    ;
							 echo "</span>";
                         }
                         
     
                         echo "<span class='DPAT'><span> Nom d'usage:</span> ";
                         if ($row['ETC_NOM_MAR']) {
                             echo $row['ETC_NOM_MAR'];
                             $signaturePhoto = $signaturePhoto.' '.$row['ETC_NOM_MAR']  ;
                         }
                         echo "</span>";
     
                         echo "<span class='DPAT'><span> Prénom: </span> ";
                         if ($row['ETC_PRN']) {
                             echo $row['ETC_PRN'];
                             $signaturePhoto = $signaturePhoto.' '.$row['ETC_PRN'];
                         }
                         echo "</span>";
     
                         echo "<span class='DPAT'><span> Sexe: </span> ";
                         if ($row['ETC_SEX']) {
                            echo $row['ETC_SEX'];
                         }
                         echo "</span>";
     
                         echo "<span class='DPAT'><span> Date de Naissance: </span>";
                         if ($row[4]) {
                             echo $row[4];
                             $signaturePhoto = $signaturePhoto.' '.$row[4];
                         }
                         echo "</span>&nbsp;&nbsp;";
                         
                         echo "<span class='DPAT'><span><a class='visucourrier' href='http://visucourrier2?person=" . $ipp . "'>Consulter VisuCourrier</a></span></span>";
                         
                        $nbsearch ++;
                        
                        }
                        if ($nbsearch == 0){
                            echo "<script type='text/javascript'> alert('Le numéro unique du patient que vous utilisez n\’est pas connu de la base du CH Valenciennes ou n\’est pas conforme.
                                    Veuillez contacter le service DESK de la DSI.')</script>";
                        }
     echo "</div>";
?>